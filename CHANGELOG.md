## 1.0.4 [06-12-2019]
* Bug fixes and performance improvements

See commit d5b82c5

---

## 1.0.3 [06-06-2019]
* Bug fixes and performance improvements

See commit 52120a2

---

## 1.0.2 [06-06-2019]
* Bug fixes and performance improvements

See commit 7bdaddd

---

## 1.0.1 [06-06-2019]
* Bug fixes and performance improvements

See commit 51c6d8b

---
