FROM golang:alpine AS build-stage
COPY . /go/src/main
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main /go/src/main

FROM scratch
COPY --from=build-stage /go/main .
CMD ["./main"]
