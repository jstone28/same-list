![](https://gitlab.com/jstone28/same-list/uploads/633b7808cd07c08a0e52a4b1853c8501/Screen_Shot_2019-06-06_at_1.31.41_PM.png)

# same-list

The same-list excercise checks that two lists are the same by verifying that all elements in one list exist in another

## Getting Started

To run this application, clone the project and run:

`go test -v .`

That will kick off a set of tests that verify the functionality of `SameListFinder()`

*An output of this command can also be found in the pipelines (CI/CD on the left side navigation bar)*

## Problem

**Given two lists, write a function that answer if all elements of one list are in the other.**

## Solution Overview

This solution takes into account the reoccurances of items in the list comparison. The executed solution looks like this:

  *Note that the order of parameters to this function matters. We're explicitly comparing the first list to the second list such that the first is a subset or identical to the second.*

1. Take all the items from the second list (the list we're comparing against), and stick that into a map. While we're populating that map, we're counting the occurances of each item and appending that as the value in the key value pair.

2. Next, we loop the first list, passing each element into the map made in the previous step. If that map doesn't contain that element, it returns false otherwise it decrements the count on the existing map element. Finally, if everything matches from both lists, we return true.

*The assumptions made in this solution are expounded upon in the next section*

## Thoughts During Exercise

* I'm curious if this questions means that the lists are identical or just that one is contained in the other (subset)? For the sake of this exercise, I am going to operate under the assumption that one list contains all the elements from another (subset) and not that they are identical.

* I also wonder about duplicates; if I have a an array `{ 1, 2, 2 }` is that the same (in the results of this function) as `{ 1, 2 }` or is the number of 2's significant. I am going to continue on as if those two arrays do contain all the elements from each other, and would return true from our function

* There's a couple ways to solve this, the two that come to mind is the tradition way of using two loops and checking each item (this is the way I'm going to implement it). However, depending on the data structure constraints, you may also be able to sort and deduplicate the lists, and then use reflect's `DeepEquals()`

* I wouldn't usually push directly on master but because this is a fairly straight forward and finite project :sunglasses:
