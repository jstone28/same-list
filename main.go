package main

import (
	"fmt"
)

// SubListFinder takes two lists and confirms the first list is a subset (or
// identical); note that the order of parameters to this function matters
func SubListFinder(arr1, arr2 []int) bool {
	// create a new int[] map
	mapList := make(map[int]int)
	// for all the items in arr2, we set that into a map and append a count
	// (to count for multiples of some or all numbers)
	for _, value := range arr2 {
		mapList[value]++
	}

	// for all the items in arr1
	for _, val := range arr1 {
		// we pass the same val into mapList and see what comes back
		if v, exists := mapList[val]; !exists {
			// if the value doesn't exist in the map; return false
			return false
			// if the count stored under the value's route is < 1; return false
		} else if v < 1 {
			return false
		} else {
			// if it does exist, decrement the count
			mapList[val] = v - 1
		}
	}
	// if we are able to loop through the arr1 without breaking out, we return
	// true, arr1 is a subset of arr2 inclusive of count
	return true
}

func main() {
	fmt.Println("Same List Finder: v1")
}
