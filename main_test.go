package main

import (
	"testing"
)

// assertEquals is a basic assertEquals implementation
func assertEquals(t *testing.T, foo interface{}, bar interface{}) {
	if foo != bar {
		t.Errorf("%v != %v", foo, bar)
	}
}

// TestSubListFinder is a happy path test of two identical arrays
func TestSubListFinder(t *testing.T) {
	array1 := []int{1, 2, 3}
	array2 := []int{1, 2, 3}

	assertEquals(t, SubListFinder(array1, array2), true)
}

// TestSubListFinderNotEqual is a negative test of two different arrays
func TestSubListFinderNotEqual(t *testing.T) {
	array1 := []int{10, 20, 30}
	array2 := []int{1, 2, 3}

	assertEquals(t, SubListFinder(array1, array2), false)
}

// TestSubListNotEqualTwo is a negative test of two different arrays
func TestSubListNotEqualTwo(t *testing.T) {
	array1 := []int{10, 20, 30, 40 ,50, 60}
	array2 := []int{1, 2, 3, 4, 5, 6}

	assertEquals(t, SubListFinder(array1, array2), false)
}

// TestSubListNotEqualThree is a negative test of two different arrays
func TestSubListNotEqualThree(t *testing.T) {
	array1 := []int{5, 4, 3, 2 ,1, 0}
	array2 := []int{1, 2, 3, 4, 5, 6}

	assertEquals(t, SubListFinder(array1, array2), false)
}

// TestSubListNotEqualFour is a negative test of two different arrays
func TestSubListNotEqualFour(t *testing.T) {
	array1 := []int{10}
	array2 := []int{1, 2, 3, 4, 5, 6}

	assertEquals(t, SubListFinder(array1, array2), false)
}

// TestSubListFinderEmptyArr1List tests that an empty list is a subset of
// a populated list
func TestSubListFinderEmptyArr1List(t *testing.T) {
	array1 := []int{}
	array2 := []int{1, 2, 3}

	assertEquals(t, SubListFinder(array1, array2), true)
}

// TestSubListFinderEqualOne tests that an empty list is a subset of
// a populated list
func TestSubListFinderEqualOne(t *testing.T) {
	array1 := []int{1}
	array2 := []int{1, 2, 3}

	assertEquals(t, SubListFinder(array1, array2), true)
}

// TestSubListFinderEqualTwo tests that an empty list is a subset of
// a populated list
func TestSubListFinderEqualTwo(t *testing.T) {
	array1 := []int{1, 2}
	array2 := []int{1, 2, 3, 50, 100, 1, 10}

	assertEquals(t, SubListFinder(array1, array2), true)
}

// TestSubListFinderEqualThree tests that an empty list is a subset of
// a populated list
func TestSubListFinderEqualThree(t *testing.T) {
	array1 := []int{1, 2}
	array2 := []int{1, 2, 3, 50, 100, 1, 10}

	assertEquals(t, SubListFinder(array1, array2), true)
}

// TestSubListFinderEqualFour tests that an empty list is a subset of
// a populated list
func TestSubListFinderEqualFour(t *testing.T) {
	array1 := []int{1, 2, 1, 2, 1, 2, 1, 2, 1, 2}
	array2 := []int{1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 50, 100, 1, 10}

	assertEquals(t, SubListFinder(array1, array2), true)
}

// TestSubListFinderEmptyArr2List tests that a populated list is not a subset of
// an empty list
func TestSubListFinderEmptyArr2List(t *testing.T) {
	array1 := []int{1, 2, 3}
	array2 := []int{}

	assertEquals(t, SubListFinder(array1, array2), false)
}
